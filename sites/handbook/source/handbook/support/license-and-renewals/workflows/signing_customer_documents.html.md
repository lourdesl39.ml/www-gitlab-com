---
layout: handbook-page-toc
title: Requests for GitLab to sign documents
category: General
description: How to deal with customers who request GitLab sign some documents.
---

From time to time, a customer may submit a ticket requesting that someone from
GitLab sign some kind of document. Examples include:

* Assignment of agreements, contracts and related documents to a different entity
* Government forms

If the documents are related to a subscription, license or sales opportunity,
Sales team members are responsible for communication with the customer. Follow
the [working with sales workflow](working_with_sales.html) to get the customer's
Account Owner involved.

Additional information on this topic can be found in the following handbook pages:

* [Sales Guide: Collaborating with GitLab Legal - How to reach Legal](/handbook/legal/customer-negotiations/#how-to-reach-legal)
* [Sales Order Processing - Contact Legal](/handbook/sales/field-operations/order-processing/#contact-legal)

Other types of documents may be covered by the [general Support workflows](/handbook/support/workflows/).
